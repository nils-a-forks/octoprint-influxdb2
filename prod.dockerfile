FROM octoprint/octoprint

ARG install_target
RUN pip install "${install_target}"
